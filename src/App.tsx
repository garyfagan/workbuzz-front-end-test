import React from 'react'
import { CssBaseline, ThemeProvider, Container } from '@mui/material'

import { theme } from './theme/theme'
import Header from './components/Header/Header'
import Keys from './components/Keys/Keys'

const App: React.FC = () => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Header />
      <Container>
        <Keys />
      </Container>
    </ThemeProvider>
  )
}

export default App
