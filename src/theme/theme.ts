import { createTheme } from '@mui/material'

export const theme = createTheme({
  palette: {
    background: {
      default: '#FFF'
    }
  },
  components: {
    MuiContainer: {
      styleOverrides: {
        root: {
          color: '#000'
        }
      }
    }
  }
})
