import React from 'react'
import type { ComponentStory, ComponentMeta } from '@storybook/react'

import Welcome from './Header'

export default {
  title: 'Example/Welcome',
  component: Welcome
} as ComponentMeta<typeof Welcome>

const Template: ComponentStory<typeof Welcome> = (args) => <Welcome />

export const Default = Template.bind({})
