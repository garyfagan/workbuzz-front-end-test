import React from 'react'
import { Container, Grid, Typography } from '@mui/material'
import image from '../../assets/image.svg'

import { Navigation } from './Header.s'

const Header: React.FC = () => {
  return (
    <Navigation>
      <Container>
        <Grid container alignItems="center" justifyContent="space-between">
          <Grid item>
            <img src={image} alt="logo" width={200} />
          </Grid>
          <Grid item>
            <Typography color="white" variant="h5">
              Front End Technical Test
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </Navigation>
  )
}

export default Header
