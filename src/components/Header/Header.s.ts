
import { Box, styled } from '@mui/material'

export const Navigation = styled(Box)({
  backgroundColor: '#0047f9',
  padding: '20px 0'
})
