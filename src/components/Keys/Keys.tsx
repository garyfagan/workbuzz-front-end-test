import React from 'react'
import { Box, Typography } from '@mui/material'

const Keys: React.FC = () => {
  return (
    <Box display="flex" alignItems="center" flexDirection="column" gap={5} mt={5}>
      <Typography>
        VITE_MARVEL_PUBLIC_KEY: {import.meta.env.VITE_MARVEL_PUBLIC_KEY}
      </Typography>
      <Typography>
        VITE_MARVEl_PRIVATE_KEY: {import.meta.env.VITE_MARVEl_PRIVATE_KEY}
      </Typography>
    </Box>
  )
}

export default Keys
